CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module helps to create country specific Blocks. It Add country
 setting to block and manages country specific display of block.

Block will be only visible for the selected countries. It detects
 and gets User's country from Ip2Country information and based on
 this it manages block visibility.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/block_country

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/block_country

REQUIREMENTS
------------

This module requires the following modules:

 * IP-based Determination of a Visitor's Country
 (https://www.drupal.org/project/ip2country)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.

CONFIGURATION
------------

The module has no menu or modifiable settings. To add country
 specific visibility to a block, go to that block's
 configuration page and in visibility section, under country
 tab, select a country.

 Check "Negate the condition" option, to get the block visible
 in all countries but the selected country.

MAINTAINERS
-----------

Current maintainers:
 * Ashish Dalvi (Ashish.Dalvi) - https://www.drupal.org/u/ashishdalvi
 * Naveen Valecha (naveenvalecha) - https://www.drupal.org/u/naveenvalecha

This project has been sponsored by:
 * BLISSTERING SOLUTIONS
   Blisstering Solutions is a Drupal Services, Solutions and Products Company.
   It offer full range of Drupal Services from building your Drupal Solution -
   that include custom module development, theming, performance or testing – to
   extending your Drupal site or solution to mobile, tablet, Facebook and cloud.
 * QED42 ENGINEERING PRIVATE LTD.

